:set relativenumber
:set number                 " Display line numbers on the left side
:set encoding=utf-8         " UTF8 Support
:set ls=2                   " This makes Vim show a status line even when only one window is shown
:filetype plugin on         " This line enables loading the plugin files for specific file types
:set tabstop=8              " Set tabstop to tell vim how many columns a tab counts for. Linux kernel code expects each tab to be eight columns wide.
:set expandtab              " When expandtab is set, hitting Tab in insert mode will produce the appropriate number of spaces.
:set softtabstop=4          " Set softtabstop to control how many columns vim uses when you hit Tab in insert mode. If softtabstop is less than tabstop and expandtab is not set, vim will use a combination of tabs and spaces to make up the desired spacing. If softtabstop equals tabstop and expandtab is not set, vim will always use tabs. When expandtab is set, vim will always use the appropriate number of spaces.
:set shiftwidth=4           " Set shiftwidth to control how many columns text is indented with the reindent operations (<< and >>) and automatic C-style indentation.
:setlocal foldmethod=indent " Set folding method
:set nofoldenable           " disable folding 
:set t_Co=256               " makes Vim use 256 colors
:set nowrap                 " Don't Wrap lines!
:set bg=dark                " set background = dark
:set nocp                   " This changes the values of a LOT of options, enabling features which are not Vi compatible but really really nice
:set clipboard=unnamed
:set clipboard=unnamedplus
:set autoindent             " Automatic indentation
:set cindent                " This turns on C style indentation
:set si                     " Smart indent
:syntax enable              " syntax highlighting
:set showmatch              " Show matching brackets
:set hlsearch               " Highlight in search
:set ignorecase             " Ignore case in search
:set noswapfile             " Avoid swap files

:set mouse=a                " Mouse Integration

:command! Wq :wq
:command! W :w
:command! Q :q
cnoremap SW execute 'silent! write !sudo tee % >/dev/null' <bar> edit!<CR>

vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
"imap <C-v> <C-r><C-o>+

" use real tabs for these languages
autocmd FileType go,make,c
            \ setlocal tabstop=8 |
            \ setlocal shiftwidth=8

" use two spaces for these languages
autocmd FileType ruby,html,json,jade,javascript
            \ setlocal tabstop=2 |
            \ setlocal shiftwidth=2

" use four spaces for these languages
autocmd FileType css
            \ setlocal tabstop=4 |
            \ setlocal shiftwidth=4


" auto complete for ( , " , ' , [ , {
:inoremap        (  ()<Left>
:inoremap        "  ""<Left>
:inoremap        `  ``<Left>
:inoremap        '  ''<Left>
:inoremap        [  []<Left>
:inoremap      {  {}<Left>

" Shortcut to rapidly toggle `set list`
nmap <C-l> :set list!<CR>

" Use the same symbols as TextMate for tabstops and EOLs
set listchars=tab:▸\ ,eol:¬

" auto comment and uncooment with F6 and F7 key
:map <C-_> :Commentary<cr><C-n>
:imap <C-_> <Esc>:Commentary<cr><C-n><i>

":noremap <silent> #5 :!gnome-terminal --title "%" --hold -e "./%" <CR> <CR>" execute bash & python script with F5
autocmd FileType sh,ruby,python :map <silent> <f5> <Esc>:w<CR>:!chmod 755 % && clear && ./% && echo -e "\npress enter to contenue \c" && read <CR> <CR>" execute bash & python script with F5
autocmd FileType c :map <silent>  <f5> <Esc>:w<CR>:!gcc % -o `sed 's/.c//g' <<< %` && clear && ./`sed 's/.c//g' <<< %`  && echo -e "\npress enter to contenue \c" && read <CR> <CR>" execute c script with F5
autocmd FileType cpp :map <silent>  <f5> <Esc>:w<CR>:!g++ % -o `sed 's/.cpp//g' <<< %` && clear && ./`sed 's/.cpp//g' <<< %`  && echo -e "\npress enter to contenue \c" && read <CR> <CR>" execute cpp script with F5
autocmd FileType go :map <silent>  <f5> <Esc>:w<CR>:!clear;go run % && echo -e "\npress enter to contenue \c" && read <CR> <CR>" execute cpp script with F5

:noremap <silent> #3 :bp<CR> " switch to pervious tab with F3
:noremap <silent> #4 :bn<CR> " switch to next tab with F2
:imap <F8> :setlocal spell! spelllang=en_us<CR> " check spelling with F8
:set pastetoggle=<F2> " Paste mode toggle with F2 Pastemode disable auto-indent and bracket auto-compelation and it helps you to paste code fro elsewhere .
:inoremap <c-s> <Esc>:w<CR>

" Base64 decode word under cursor
nmap <Leader>b :!echo <C-R><C-W> \| base64 -d<CR>

" grep recursively for word under cursor
nmap <Leader>g :tabnew\|read !grep -Hnr '<C-R><C-W>'<CR>

" sort the buffer removing duplicates
nmap <Leader>s :%!sort -u --version-sort<CR>

" Function to install vim-plug if not already installed
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -sfLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    echo "vim-plug installed"
    " Run :PlugInstall to install plugins
    autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-plug'
" Plug 'prabirshrestha/vim-lsp'
" Plug 'mattn/vim-lsp-settings'
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'morhetz/gruvbox'
Plug 'vim-scripts/taglist.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Plug 'enricobacis/vim-airline-clock'
Plug 'scrooloose/nerdtree'
Plug 'Yggdroot/indentLine'
Plug 'vim-scripts/Pydiction'
Plug 'vim-scripts/L9'
Plug 'vim-scripts/indentpython.vim'
Plug 'vim-scripts/AutoComplPop'
Plug 'vim-scripts/SearchComplete'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-syntastic/syntastic'
Plug 'editorconfig/editorconfig-vim'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-commentary'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
call plug#end()

:colorscheme  gruvbox       " Set colorScheme

" for transparent background
highlight Normal guibg=NONE ctermbg=NONE

"Syntastic recommended settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

"ctrlp map to Ctrl+t
let g:ctrlp_map = '<c-t>'

"indentLine 
:let g:indentLine_char = '│'

" airline plugin setting
:let g:airline_theme='luna' " set airline plugin theme
:let g:airline#extensions#tabline#enabled = 1 " showing tabs 
:let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'

:let g:airline#extensions#clock#format = '%c'

" NERDTree plugin setting
:map <C-b> :NERDTreeToggle<CR> " toggle showing NERDTree
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let NERDTreeShowHidden=1 " Show hidden files
"open a NERDTree automatically when vim starts up if no files were specified
:autocmd StdinReadPre * let s:std_in=1
:autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
:autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif " close vim if the only window left open is a NERDTree
let g:pydiction_location = '~/.vim/Pydiction/complete-dict'
let g:pydiction_menu_height = 3

" coc configs
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
" All of your Plugins must be added before the following line
filetype plugin indent on    " required
