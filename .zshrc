# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export PATH=$PATH:$HOME/.bin:$HOME/.local/bin:$HOME/go/bin

# Set the directory we want to store zinit and plugins
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ]; then
   mkdir -p "$(dirname $ZINIT_HOME)"
   git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi
# Source/Load zinit
source "${ZINIT_HOME}/zinit.zsh"

# Add in Powerlevel10k
zinit ice depth=1; zinit light romkatv/powerlevel10k

# Add in zsh plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light MichaelAquilina/zsh-auto-notify
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions
zinit light Aloxaf/fzf-tab

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Add in snippets
zinit snippet OMZP::git
zinit snippet OMZP::sudo
zinit snippet OMZP::archlinux
zinit snippet OMZP::aws
zinit snippet OMZP::kubectl
zinit snippet OMZP::kubectx
zinit snippet OMZP::laravel
zinit snippet OMZP::laravel5
zinit snippet OMZP::colored-man-pages
zinit snippet OMZP::command-not-found
zinit load zsh-users/zsh-history-substring-search
zinit load ariaieboy/laravel-sail
zinit ice wait atload'_history_substring_search_config'

# Load completions
autoload -Uz compinit && compinit

# Keybindings
bindkey -e
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey '^@' autosuggest-accept

bindkey '^[w' kill-region
bindkey '^[[3~' delete-char
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

bindkey -s ^f "tmux-sessionizer\n"
bindkey -s ^h "fast-ssh.sh\n"

AUTO_NOTIFY_IGNORE+=("lf" "ssh")
PKGFILE_PROMPT_INSTALL_MISSING=true

# History
HISTSIZE=10000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

unsetopt nomatch
setopt interactivecomments

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath' # Configure completion for scp


# Include hosts from ~/.ssh/config
# zstyle ':completion:*:hosts' hosts $(awk '/^Host / { print $2 }' ~/.ssh/config)
#
# zstyle ':completion:*:*:scp:*' tag-order 'hosts'
# zstyle ':completion:*:hosts' hosts 'my-server' 'other-server'

# Aliases
alias ariadl='aria2c -c -j1 -x16 -s16 -k1M -d ~/Downloads'
alias df="pydf"
alias :q="exit"
alias ls='ls --color'
alias ll='exa -lg --sort=modified --icons'
alias tree='exa --tree'
alias top="bpytop"
alias clea='clear'
alias zs="source ~/.zshrc"
alias linbox="wine ~/.Apps/winbox.exe"
alias ydl="yt-dlp --proxy socks5://127.0.0.1:7890 -ico \"~/Downloads/%(title)s.%(ext)s\""
alias sail='sh $([ -f sail ] && echo sail || echo vendor/bin/sail)'
alias qrc='qrencode -o- -t ANSI256UTF8'

# Aliases
unalias gau
unalias gf

if [ -f /usr/bin/nvim ];then
    alias vi="nvim"
    export EDITOR="nvim"
fi

# Shell integrations
eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"

if [ $TERM  = "xterm-kitty" ]; then
    alias icat="kitty +kitten icat"
    # alias ssh="kitty +kitten ssh"
    alias ssh="TERM=xterm  /usr/bin/ssh"
fi

if [ $TERM  = "xterm-ghostty" ]; then
    # alias icat="kitty +kitten icat"
    # alias ssh="kitty +kitten ssh"
    alias ssh="TERM=xterm  /usr/bin/ssh"
fi

# if $TERM_PROGRAM is set
if [[ -n "$TERM_PROGRAM" && "$TERM_PROGRAM" = "WezTerm" ]]; then
    alias icat="wezterm imgcat"
    # alias ssh="wezterm ssh"
fi

#add extra private configs ;)
if [[ -s ~/.zshrc_extra ]]; then
    source ~/.zshrc_extra
fi

# bun completions
[ -s "/home/mk/.bun/_bun" ] && source "/home/mk/.bun/_bun"
