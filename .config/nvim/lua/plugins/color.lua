return {
    -- add gruvbox
    { "ellisonleao/gruvbox.nvim" },
    { "eandrju/cellular-automaton.nvim" },
    {
        "rose-pine/neovim",
        disable_background = true,
    },
    -- Configure LazyVim to load gruvbox
    {
        "tokyonight.nvim",
        opts = {
            transparent = true,
            styles = {
                sidebars = "transparent",
                floats = "transparent",
            },
        },
    },
    -- {
    --   "LazyVim/LazyVim",
    --   opts = {
    --     colorscheme = "rose-pine",
    --   },
    -- },
}
