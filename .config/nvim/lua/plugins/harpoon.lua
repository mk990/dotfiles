return {
    "ThePrimeagen/harpoon",
    lazy = false,
    dependencies = {
        "nvim-lua/plenary.nvim",
    },
    config = true,
    keys = {
        { "<C-e>", "<cmd>lua require('harpoon.ui').toggle_quick_menu()<cr>", desc = "Show harpoon marks" },
        { "<leader>a", "<cmd>lua require('harpoon.mark').add_file()<cr>", desc = "Mark file with harpoon" },
        { "<C-h>", "<cmd>lua require('harpoon.ui').nav_file(1)<cr>", desc = "go to file 1" },
        { "<C-t>", "<cmd>lua require('harpoon.ui').nav_file(2)<cr>", desc = "go to file 2" },
        { "<C-n>", "<cmd>lua require('harpoon.ui').nav_file(3)<cr>", desc = "go to file 3" },
        { "<C-s>", "<cmd>lua require('harpoon.ui').nav_file(4)<cr>", desc = "go to file 4" },
        { "<leader>hn", "<cmd>lua require('harpoon.ui').nav_next()<cr>", desc = "Go to next harpoon mark" },
        { "<leader>hp", "<cmd>lua require('harpoon.ui').nav_prev()<cr>", desc = "Go to previous harpoon mark" },
    },
}
