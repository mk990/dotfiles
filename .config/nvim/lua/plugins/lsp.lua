--  .config/neovim/lua/pluging/lsp.lua
return {
    {
        "neovim/nvim-lspconfig",
        opts = {
            servers = {
                solidity = {
                    cmd = {
                        "autovarphp",
                        "--stdio",
                    },
                    filetypes = { "php" },
                    root_dir = require("lspconfig").util.find_git_ancestor,
                    single_file_support = true,
                },
            },
            completion = {
                completeopt = "menuone,noselect",
                keyword_pattern = "\\k+",
                keyword_length = 1,
            },
        },
    },
}
