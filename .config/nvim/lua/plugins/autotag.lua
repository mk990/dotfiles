return {
    {
        "nvim-treesitter/nvim-treesitter",
        dependencies = {
            "windwp/nvim-ts-autotag",
        },
        opts = {
            ensure_installed = {
                -- defaults
                "vim",
                "lua",
                "markdown",
                "html",
                "css",
                "javascript",
                "typescript",
                "json",
                "c_sharp",
                "sql",
                "yaml",
                "xml",
                "tsx",
            },
            autotag = {
                enable = true,
            },
        },
    },
}
