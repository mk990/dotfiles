#!/usr/bin/env bash
LANGUAGES="php lua rust java javascript typescript golang"
COMMANDS="awk sed find grep"
SELECTED=$(echo "$LANGUAGES" "$COMMANDS" | sed 's/\s/\n/g' | fzf)
if [[ -z $SELECTED ]]; then
	exit 0
fi

read -p "Enter Query: " -r QUERY
QUERY=$(echo "$QUERY" | tr ' ' '+')
if echo "$LANGUAGES" | grep -q "$SELECTED"; then
	tmux neww bash -c "echo \"curl cht.sh/$SELECTED/$QUERY/\" & curl -s cht.sh/$SELECTED/$QUERY & while [ : ]; do sleep 1; done"
else
	tmux neww bash -c "echo \"curl cht.sh/$SELECTED~$QUERY/\" & curl -s cht.sh/$SELECTED~$QUERY & while [ : ]; do sleep 1; done"
fi
