#!/bin/bash

SSH=$(cat ~/.ssh/config | grep -i "Host " | cut -f2 -d" " | sort | fzf)
TERM=xterm
clear

if [ "$SSH" == "" ]; then
	exit
fi
echo ssh "$SSH"
ssh "$SSH"
