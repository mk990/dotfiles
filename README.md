My configs for GNU/linux

# Requirements

Install tmux and zsh most

Debian and Ubuntu
```sh
sudo apt install tmux zsh vim most exuberant-ctags eza zoxide stow
```

Arch
```sh
sudo pacman -S zsh tmux neovim most ctags stow eza zoxide
```

# Installing
 1. git clone https://gitlab.com/mk990/dotfiles.git ~/.dotfiles
 2. cd ~/.dotfiles/ 
 3. stow .
 
 Done
