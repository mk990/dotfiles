#!/bin/bash
cd $HOME
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
sed 's/:colorscheme/":colorscheme/g' -i ~/.vimrc
vim +PlugInstall +qall
sed 's/":colorscheme/:colorscheme/g' -i ~/.vimrc
cp -r ~/.vim/gruvbox/colors ~/.vim/
ln -sf ~/.dotfiles/coc-settings.json ~/.vim/coc-settings.json
